import sass from 'rollup-plugin-sass';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import minify from 'rollup-plugin-babel-minify';
import html from 'rollup-plugin-fill-html';

export default [
    configuration({
        inputs: {
            script: './src/app/index.js',
            dom: 'src/index.html'
        },
        outputs: {
            script: 'bin/app.js',
            style: 'bin/app.css'
        },
    })
];

function configuration(io) {
    const c = {
        input: io.inputs.script,
        output: {
            file: io.outputs.script,
            format: 'iife',
            sourcemap: false,
        },
        plugins: []
    };

    if (io.outputs.style) {
        c.plugins.push(
            sass({
                output: io.outputs.style
            })
        );
    }

    c.plugins.push(
        babel({
            exclude: 'node_modules/**',
            babelrc: false,
            presets: [['@babel/env']]
        })
    );

    c.plugins.push(
        resolve({
            browser: true,
        })
    );

    c.plugins.push(
        minify({
            comments: false,
            sourceMap: false,
        })
    );

    if (io.inputs.dom) {
        c.plugins.push(
            html({
                template: io.inputs.dom,
                filename: io.outputs.dom || 'index.html'
            })
        );
    }

    return c;
}
